#!/usr/bin/env bash

currentDate=$(date +"%T")
helm package *-configmap
#helm repo index ./ --url https://edicsonm.github.io/chubb-charts/
helm repo index ./ --url https://bitbucket.org/edimoto/chubb-charts/raw/master/
git add .
git commit -m "Generating new distribution at $currentDate"
git push origin master